#!/usr/bin/env python
# v1.0 created 2024-03-08

"""add_tabular_hits_to_fasta.py v1.0  last modified 2024-03-08
add_tabular_hits_to_fasta.py -i test_csv_for_annotation_script.tab -q test_fasta_for_annotation_script.faa > test_fasta_with_annotations.faa

  expects -i in format of:
SRR12647630_contig_45978_9_565	Glyoxalase
  
  expects -q in format of:
>SRR12647630_contig_4735_1_2521 [Blepharisma undulans]

"""

import sys
import argparse
import time

def clean_header_line(genedesc):
	genedesc = genedesc.replace("3'","3-prime")
	genedesc = genedesc.replace("5'","5-prime")
	genedesc = genedesc.replace("G(s)", "G_s")
	genedesc = genedesc.replace("G(q)", "G_q")
	genedesc = genedesc.replace("G(k)", "G_k")
	genedesc = genedesc.replace(" [GTP]","")
	genedesc = genedesc.replace(" [ubiquinone]","")
	genedesc = genedesc.replace(" [glutamine-hydrolyzing]","")

	genedesc = genedesc.replace("(","_")
	genedesc = genedesc.replace(")","_")
	genedesc = genedesc.replace("'","")
	genedesc = genedesc.replace("[","")
	genedesc = genedesc.replace("]","")
	genedesc = genedesc.replace(";","_")
	genedesc = genedesc.replace(":","_")
	genedesc = genedesc.replace(",","_")
	genedesc = genedesc.replace("/","_")
	return genedesc
	
def read_tabular_descriptions(tabularfile):
	hitcounter = 0
	best_hit_dict = {}
	sys.stderr.write( "# Reading description list from {}  {}\n".format( tabularfile , time.asctime() ) )
	for line in open( tabularfile ,'rt'):
		hitcounter += 1
		lsplits = line.strip().split("\t")
		queryid = lsplits[0]
		description = lsplits[1].strip()
		description_clean = clean_header_line(description)
		best_hit_dict[queryid] = description_clean
	sys.stderr.write( "# Counted {} descriptions for {} unique queries\n".format(hitcounter, len(best_hit_dict) ) )
	return best_hit_dict

def main(argv, wayout):
	if not argv:
		argv.append("-h")
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', help="tabular gene descriptions", required=True)
	parser.add_argument('-q','--query', help="query proteins in fasta format")
	args = parser.parse_args(argv)
	
	best_hit_dict = read_tabular_descriptions(args.input)

	query_count = 0
	for line in open(args.query):
		if line.strip() and line[0]==">":
			query_count += 1
			queryid , species = line.strip().split(' ',1)
			best_hit = best_hit_dict.get(queryid[1:], "NO_HIT")
			outstring = "{} {} {}\n".format( queryid, best_hit , species )
			wayout.write(outstring)
		else:
			wayout.write(line)

	sys.stderr.write( "# Counted {} queries from {}  {}\n".format( query_count, args.query , time.asctime() ) )


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
