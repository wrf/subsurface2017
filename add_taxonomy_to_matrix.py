#!/usr/bin/env python
#
# add_taxonomy_to_matrix.py  created 2021-11-30

'''add_taxonomy_to_matrix.py  last modified 2021-11-30

    requires NCBI Taxonomy database from
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz

add_taxonomy_to_matrix.py fullnamelineage.dmp matrix.tab > matrix_w_lineage.tab


'''

import sys
import csv
import os

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	sp_to_lineage = {} # key is species, value is full lineage line
	
	fullnamelineage = sys.argv[1]
	sys.stderr.write("# Reading lineages from {}\n".format(fullnamelineage) )
	for line in open(fullnamelineage,'r'):
		line = line.strip()
		lsplits = [s.strip() for s in line.split("|")]
		species_name = lsplits[1]
		full_lineage = lsplits[2]
		if full_lineage: # ignore top level
			sp_to_lineage[species_name] = full_lineage
	sys.stderr.write("# Found {} entries to with lineage\n".format( len(sp_to_lineage) ) )
	
	line_count = 0
	found_count = 0
	not_found_count = 0
	
	matrix_csv = sys.argv[2]
	sys.stderr.write("# Reading {}\n".format( sys.argv[2] ) )
	for line in open(matrix_csv,'r'):
		line = line.strip()
		if line:
			line_count += 1
			lsplits = line.split("\t")
			species = lsplits[2]
			if species=="Species": # meaning header line
				full_lineage = "Full Lineage"
			else:
				full_lineage = sp_to_lineage.get(species,None)

			if full_lineage is None: # count found and not found
				not_found_count += 1
				full_lineage = "Unknown lineage"
			else:
				found_count += 1

			outsplits = lsplits[0:3] + [full_lineage] + lsplits[3:]
			outline = "{}\n".format( "\t".join(outsplits) )

			sys.stdout.write(outline)

	sys.stderr.write("# Counted {} lines, found lineage for {}, missing for {}\n".format( line_count, found_count, not_found_count ) )


