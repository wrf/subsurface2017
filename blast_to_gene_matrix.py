#!/usr/bin/env python
#
# blast_to_gene_matrix.py v1 created by WRF 2017-02-10
# v1.4 2021-11-30  python3 updates
# v1.5 2022-03-30  optional column for KOG

'''blast_to_gene_matrix.py v1.5 2022-03-30

    convert tabular blast output into matrix of samples to gene counts
    like:
        PM5     PM30      PM70    PM90   PM159
  Gene1  #       #         #       #      #
  Gene2  #       #         #       #      #

    sample information is extracted from the 1st term in the query
  PM159_NoIndex_L004_R1_001_(paired)_trimmed_(paired)_contig_6_[cov=14968]

    sample is PM159

blast_to_gene_matrix.py -i metagenome_blastx_output.tab > sample_gene_matrix.tab


  -f  to add gene description and species
    use -f to include a GenBank style FASTA file

blast_to_gene_matrix.py -i metagenome_blastx_output.tab -f SEED_db.fasta > sample_gene_matrix.tab


  -p  to add signalP hits as .csv file of genename
blast_to_gene_matrix.py -i metagenome_blastx_output.tab -f SEED_db.fasta -p metagenome_signalp.csv > sample_gene_matrix.tab


  -e  to remove a predefined list of entries as they are encountered
    such as known contaminants
    use -e contaminant_list.txt  where each line is a search term
    NOTE: DO NOT USE COMMON ITEMS IN THE EXCLUSION LIST as this will remove most lines
    such as: Species Candidatus cluster archaeon

  -t  to add string of full lineage from NCBI Taxonomy
    this first requires NCBI Taxonomy database from:
    wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz
    
    that database is updated often, so it is advisable to name the folder by date
    the given file must always be fullnamelineage.dmp

  -k  to add column of KOG annotations
    
blast_to_gene_matrix.py -i metagenome_blastx_output.tab -f SEED_db.fasta -t ~/ncbi_taxonomy_20210518/fullnamelineage.dmp > sample_gene_matrix.tab

'''

import sys
import argparse
import time
import re
from collections import defaultdict

def read_cazy_fasta(cazy_fasta):
	entry_counter = 0
	gi_to_cazy_number = {} # keys are NCBI accessions, values are CAZY GH numbers
	sys.stderr.write( "# Reading CAZY proteins file {}  {}\n".format(cazy_fasta, time.asctime() ) )
	for line in open(cazy_fasta, 'r'):
		if line[0]==">":
			entry_counter += 1
			#gi|474338|gb|AAA17751.1| alpha-amylase [AAA17751.1;GH13
			# gene                    desc           annotation gh_number
			gene, desc = line[1:].strip().split(" ",1) # gene is everything before first space
			annotation, cazy_id = desc.split("[",1)
			gh_number = cazy_id.split(";")[1]
			gi_to_cazy_number[gene] = [gh_number,annotation]
			if entry_counter < 2:
				sys.stderr.write( "Entries as {}:{} {}\n".format(gene, gh_number, annotation) )
	sys.stderr.write( "# Counted {} CAZY entries  {}\n".format(entry_counter, time.asctime() ) )
	return gi_to_cazy_number

def read_cazy_blast(cazy_blast_table):
	hit_counter = 0
	gene_to_gi = {} # key is gene accession ID, value is NCBI GI number blast hit
	sys.stderr.write( "# Reading CAZY blast results {}  {}\n".format(cazy_blast_table, time.asctime() ) )
	for line in open(cazy_blast_table, 'r'):
		line = line.strip()
		if line and line[0]!="#":
			hit_counter += 1
			lsplits = line.split("\t")
			queryid = lsplits[0]
			subjectid = lsplits[1]
			gene_to_gi[queryid] = subjectid
	sys.stderr.write( "# Counted {} blast hits against CAZY  {}\n".format(hit_counter, time.asctime() ) )
	return gene_to_gi

def read_fasta_to_description(fastafile):
	speciesdict = {}
	descdict = {}
	desc_counter = 0
	sys.stderr.write( "# Reading {}  {}\n".format(fastafile, time.asctime() ) )
	for line in open(fastafile,'r'):
		if line[0]==">":
			desc_counter += 1
			if desc_counter % 20000 == 0:
				sys.stderr.write(".")
			if desc_counter % 1000000 == 0:
				sys.stderr.write("{}  {}\n".format(desc_counter, time.asctime() ) )
			gene, desc = line[1:].split(" ",1)
			# should be rsplit, as some entries begin with additional bracket
			# such as
			# OFY39883.1 [FeFe] hydrogenase H-cluster radical SAM maturase HydG [Bacteroidetes bacterium RBG_13_44_24]
			annotation, species = desc.rsplit("[",1)
			species = species.replace("]","").strip()
			speciesdict[gene] = species
			descdict[gene] = clean_annotation(annotation) if annotation.strip() else "None"
	sys.stderr.write( "# Counted {} FASTA descriptions  {}\n".format( len(descdict), time.asctime() ) )
	return speciesdict, descdict
	
def clean_annotation(annot_string):
	'''remove symbols from fasta header annotation'''
	for symbol in "#';":
		annot_string = annot_string.replace(symbol, "")
	for symbol in "()":
		annot_string = annot_string.replace(symbol, "_")
	annot_string = annot_string.strip()
	return annot_string

def read_exclusion_list(exclude_file):
	exclude_dict = {} # key is exclude item, value is True
	sys.stderr.write("# Reading {}  {}\n".format( exclude_file, time.asctime() ) )
	for line in open(exclude_file,'r'):
		line = line.strip()
		if line:
			exclude_dict[line] = True
	sys.stderr.write("# Found {} entries to exclude  {}\n".format( len(exclude_dict), time.asctime() ) )
	return exclude_dict

def read_ncbi_full_lineage(fullnamelineage_file):
	sp_to_lin_dict = {} # key is species, value is full lineage line
	sys.stderr.write("# Reading lineages from {}  {}\n".format( fullnamelineage_file, time.asctime() ) )
	for line in open(fullnamelineage_file,'r'):
		line = line.strip()
		lsplits = [s.strip() for s in line.split("|")]
		species_name = lsplits[1]
		full_lineage = lsplits[2]
		if full_lineage: # ignore top level
			sp_to_lin_dict[species_name] = full_lineage
	sys.stderr.write("# Found {} entries with complete lineage  {}\n".format( len(sp_to_lin_dict), time.asctime() ) )
	return sp_to_lin_dict

def read_kog_annotations(kogannot_file):
	prot_to_kog_dict = {} # key is protein ID, value is KOG annotation
	sys.stderr.write("# Reading KOG annotations from {}  {}\n".format( kogannot_file, time.asctime() ) )
	for line in open(kogannot_file,'r'):
		line = line.strip()
		if line:
			lsplits = line.split("\t")
			protein_id = lsplits[0]
			annotation = lsplits[2].replace("[","__").replace("]","__")
			prot_to_kog_dict[protein_id] = annotation
	sys.stderr.write("# Found {} entries with annotations  {}\n".format( len(prot_to_kog_dict), time.asctime() ) )
	return prot_to_kog_dict

def main(argv, wayout):
	if not len(argv):
		argv.append('-h')
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('-i','--input', nargs="*", help="tabular blast output")
	parser.add_argument('-d','--delimiter', default="_", help="delimiter for splitting the sample ID [_]")
	parser.add_argument('-s','--similarity', default=60.0, type=float, help="amino acid similarity cutoff [60.0]")
	parser.add_argument('-l','--length', default=50.0, type=float, help="alignment length cutoff [50.0]")
	parser.add_argument('-e','--exclude', help="text file of species or groups to exclude - one per line in text format, must also use -f")
	parser.add_argument('-f','--fasta', help="optional FASTA file to include gene descriptions")
	parser.add_argument('-k','--KOG', help="tabular KOG annotations, KOG as 3rd column")
	parser.add_argument('-c','--cazy-blast-results', help="blast results against CAZY enzymes")
	parser.add_argument('-C','--cazy-fasta', help="FASTA format file of CAZY enzymes")
	parser.add_argument('-p','--signalp', help="optional SignalP summarized results file")
	parser.add_argument('-t','--taxonomy', help="fullnamelineage.dmp file from NCBI Taxonomy FTP to optionally add taxonomy string to table, must also use -f")
	args = parser.parse_args(argv)

	### PARSE OPTIONAL SIGNALP OUTPUT AS tab delimited file OF GENE NAMES
	if args.signalp:
		hassignal = {}
		for line in open(args.signalp,'r'):
			protid = line.split(",")[0]
			hassignal[protid] = "True"

	### READ INPUT DATA
	genecounts = defaultdict( lambda: defaultdict(int) ) # dict of dicts, keys are subject IDs, values are counts
	genelist = {} # store genes as boolean dictionary
	hitcounter = 0
	for blastfile in args.input:
		sys.stderr.write( "# Reading {}  {}\n".format(blastfile, time.asctime() ) )
		for line in open(blastfile,'r'):
			line = line.strip()
			if line and line[0]!="#":
				if float(line.split("\t")[2]) >= args.similarity and float(line.split("\t")[3]) >= args.length:
					hitcounter += 1
					lsplits = line.split("\t")
					queryid = lsplits[0]
					sampleid = queryid.split(args.delimiter)[0]
					try:
						coverage = int( re.search( "\[cov=(\d+)\]", queryid ).group(1) )
					except AttributeError:
						sys.stderr.write( "NO COVERAGE {}\n".format(queryid) )
						coverage = 1
					subjectid = lsplits[1]
					genelist[subjectid] = True
					genecounts[sampleid][subjectid] += coverage
					if args.signalp:
						if hassignal.get(queryid,False):
							hassignal[subjectid] = "True"
		sys.stderr.write( "# Counted {} BLAST hits\n".format(hitcounter, time.asctime() ) )
	sys.stderr.write( "# Found {} total genes\n".format( len(genelist) ) )

	### PARSE OPTIONAL FASTA TO GET GENE DESCRIPTIONS

	if args.fasta:
		speciesdict, descdict = read_fasta_to_description(args.fasta)

	### PARSE OPTIONAL CAZY BLAST HITS AND FASTA
	
	gi_to_cazy_number = read_cazy_fasta(args.cazy_fasta) if args.cazy_fasta else {}
	gene_to_gi = read_cazy_blast(args.cazy_blast_results) if args.cazy_blast_results else {}
	
	### PARSE EXCLUSION LIST OF CONTAMINATING SPECIES ###
	# this must use -f, since it relies on the Species names
	exclusion_count = 0
	exclusion_dict = read_exclusion_list(args.exclude) if args.exclude else {}
	
	### PARSE OPTIONAL NCBI TAXONOMY FOR FULL LINEAGE OF HITS ###
	# this also must use -f, since it relies on the Species names
	species_to_lineage_dict = read_ncbi_full_lineage(args.taxonomy) if args.taxonomy else {}

	### PARSE OPTIONAL KOG HITS
	kog_annotations = read_kog_annotations(args.KOG) if args.KOG else {}

	### PRINT MATRIX

	# header line
	lineheaders = ["Gene"]
	if args.fasta: # include gene description and species
		lineheaders.extend(["Description","Species"])
		if args.taxonomy:
			lineheaders.extend(["Full lineage"])
	if args.signalp:
		lineheaders.append("Has_signal")
	if args.cazy_blast_results and args.cazy_fasta:
		lineheaders.extend(["CAZY GH","CAZY description"])
	if args.KOG:
		lineheaders.append("KOG hit")
	sys.stdout.write( "{}\n".format( "\t".join( lineheaders + sorted( genecounts.keys() ) ) ) )

	# print each gene
	write_count = 0
	for gene in sorted( genelist.keys() ):
		countlist = [gene] # build from empty list with only gene name
		if args.fasta:
			species_name = speciesdict.get(gene,"NONE")
			if args.exclude: # check if any of the space separated names match the dictionary
				if any( species_split in exclusion_dict for species_split in species_name.split(" ") ):
					exclusion_count += 1
					# and then skip this entry and do not print
					continue
			countlist.extend([descdict.get(gene,"NONE"), species_name ])
			if args.taxonomy:
				countlist.extend([ species_to_lineage_dict.get(species_name,"NO LINEAGE") ])
		if args.signalp:
			countlist.append( hassignal.get(gene,"False") )
		if args.cazy_blast_results and args.cazy_fasta:
			ginumber = gene_to_gi.get(gene,None)
			if ginumber is None:
				sys.stderr.write( "no GI for {}\n".format(gene) )
			countlist.extend( gi_to_cazy_number.get( ginumber , ["NONE","NONE"]) )
		if args.KOG:
			countlist.append( kog_annotations.get(gene,"NONE") )
		for sk in sorted(genecounts.keys()):
			countlist.append( str(genecounts[sk][gene]) )
		write_count += 1
		sys.stdout.write( "{}\n".format( "\t".join(countlist) ) )

	if exclusion_count:
		sys.stderr.write( "# {} items had matches in the exclude list, removed from output\n".format( exclusion_count ) )
	if write_count:
		sys.stderr.write( "# {} items written \n".format( write_count ) )
	else:
		sys.stderr.write( "# WARNING: NO items written \n" )
	sys.stderr.write( "# Process completed  {}\n".format( time.asctime() ) )

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
