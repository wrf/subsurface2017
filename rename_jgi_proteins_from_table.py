#!/usr/bin/env python
#
# rename_jgi_proteins_from_table.py  created 2022-03-22

"""rename_jgi_proteins_from_table.py  last modified 2022-03-24

rename_jgi_proteins_from_table.py protein_set.fasta protein_annotations.tab > protein_set_renamed.fasta

    for example
  >jgi|PirE2_1|1063|gm1.9_g
    and
  >jgi|PirE2_1|1062|gm1.8_g

    should be printed as
  >jgi|PirE2_1|1063|gm1.9_g Proteasome endopeptidase complex. [Piromyces E2]
    or
  >jgi|PirE2_1|1062|gm1.8_g hypothetical protein [Piromyces E2]
"""

import sys
import os
import re

example_table = """#proteinId	ecNum	definition	catalyticActivity	cofactors	associatedDiseases	pathway	pathway_class	pathway_type
1063	3.4.25.1	Proteasome endopeptidase complex.	Cleavage at peptide bonds with very broad specificity.					
1085	3.6.1.3	Adenosinetriphosphatase.	ATP + H(2)O = ADP + phosphate.		Hemolytic anemia due to deficiency of adenosine triphosphatase; MIM:102800.	Purine metabolism	Nucleotide Metabolism	METABOLIC
1090	3.4.21.-	Serine endopeptidases.						
"""

example_fasta = """>jgi|PirE2_1|1062|gm1.8_g
METLEKELNQAEKFNFSFEEYIKNELNISLENNTVNVETCKYFQKGQCAKGNNCQYRHARPEKTVVCKHW
LRGLCKKGDLCEFLHEYNLKKMPECWFYSKYGECSNPECMYLHVDPESKVRECAWYARGFCKHGPNCRHK
HVRKIICQNYISGFCPKGPDCNQGHPKYELPNLNTQYFTQNQDSDENVIIL*
>jgi|PirE2_1|1063|gm1.9_g
MTVDLSEGEKLIKTSPQAAVEFYKNILSATYESKEENARAKEIAIIKLGEIYSEQKKSNELSDLIRSSRN
FLISIPKAKSAKIVRTLIDLFAKIENTLPQQVEICKESIDWAISEKRIFLKQSLQTRLISLYLDNKMYHE
ALELIDELLKELKRLDDKNVLMEVQLLESRVCHALRNFPKSRAALTSARTSANAIYCPPLMQAALDLQSG
ILHAEEKDYKTAYSYFYETLEGYSSQDDPRAVLALKYMLLCKIMLNLSDDVMTITSSKLADKYQGRSIEA
MIAIATAHKNRSLKEFEKALNSYQDELGNDIIIRSHLGTLYDTLLEQNLSKIIEPFSCVEIEHVAKLIDL
PASQVESKLSQMILDKVFSGILDQGAGCLIVFEEPPIDKTYETALETIKSMSNVVESLYDKAAQLN*
"""

VERBOSE = True

def clean_up_description(description):
	# change a bunch of disallowed symbols, or ones that cause problems later
	underscore_symbols = "(),=|#"
	for symbol in underscore_symbols:
		description = description.replace(symbol,"_")
	remove_symbols = "'[]."
	for symbol in remove_symbols:
		description = description.replace(symbol,"")
	description = description.replace("/","-")
	return description


if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	# declare both files
	annotation_table = sys.argv[2]
	fasta_file = sys.argv[1]

	# species names are normally pulled from the file name of the fasta file
	# for cases with errors, or extended strain IDs, or errors
	# add to this list of bad names
	bad_names = {"Anaeromyces_robusts_all_proteins_20160330.aa.fasta":"Anaeromyces robustus",
                 "Chytriomyces_sp_MP_71_all_proteins_20161010.aa.fasta":"Chytriomyces sp MP_71",
                 "Neocallimastix sp. WI3-B_1_all_proteins_20201119.aa.fasta":"Neocallimastix sp WI3-B",
                 "Neocallimastix_sp_GF-Ma3-1_all_proteins_20201119.aa.fasta":"Neocallimastix sp GF-Ma3-1",
                 "Orpinomyces_sp_strain_C1A_1_GeneCatalog_proteins_20140826.aa.fasta":"Orpinomyces sp strain C1A",
                 "Pecoramyces_sp_F1_GeneCatalog_proteins_20210510.aa.fasta":"Pecoramyces sp F1",
                 "Piromyces_sp_UH3-1_p0_1_all_proteins_20200513.aa.fasta":"Piromyces sp UH3-1" }

	sp_name_splits = os.path.basename(fasta_file).split("_")
	species_name = bad_names.get( os.path.basename(fasta_file) , "{} {}".format( sp_name_splits[0], sp_name_splits[1] ) )
	annotation_dict = {} # key is protein ID, value is definition, 3rd column

	linecounter = 0
	if VERBOSE:
		sys.stderr.write( "# reading annotations from {}\n".format(annotation_table) )
	for line in open(annotation_table, 'rt'):
		line = line.strip()
		if line and line[0]!="#":
			linecounter += 1
			lsplits = line.split("\t")
			proteinid = lsplits[0]
			description = lsplits[2]
			if proteinid in annotation_dict:
				continue
				#sys.stderr.write(  "ERROR: protein ID {} already found in table".format(proteinid)  )
			annotation_dict[proteinid] = clean_up_description(description)
	if VERBOSE:
		sys.stderr.write( "# found {} annotations from {} lines\n".format( len(annotation_dict), linecounter ) )

	protcounter = 0
	prots_w_annot = 0
	if VERBOSE:
		sys.stderr.write( "# reading sequences from {}\n".format(fasta_file) )
	for line in open(fasta_file, 'rt'):
		line = line.strip()
		if line and line[0]!="#":
			if line[0]==">":
				protcounter += 1
				headersplits = line.split("|")
				proteinid = headersplits[2]
				protein_desc = annotation_dict.get(proteinid, None)
				if protein_desc is None:
					protein_desc = "Hypothetical protein"
				else:
					prots_w_annot += 1
				new_outline = "{} {} [{}]\n".format(line.replace("#","-"), protein_desc, species_name)
				sys.stdout.write( new_outline )
			else:
				sys.stdout.write( "{}\n".format(line) )
	if VERBOSE:
		sys.stderr.write( "# counted {} proteins, {} had annotations\n".format(protcounter, prots_w_annot) )


#
