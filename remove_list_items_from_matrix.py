#!/usr/bin/env python
#
# remove_list_items_from_matrix.py  created 2021-11-29

'''remove_list_items_from_matrix.py  last modified 2021-11-29

remove_list_items_from_matrix.py exclusion_list.txt matrix.tab > filtered_matrix.tab

    NOTE: DO NOT USE COMMON ITEMS IN THE EXCLUSION LIST
    this will remove most lines
    such as: Species Candidatus cluster archaeon
'''

import sys
import csv
import os

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	exclude_list = {} # key is exclude item, value is True
	line_count = 0
	remove_count = 0
	
	exclude_file = sys.argv[1]
	sys.stderr.write("# Reading {}\n".format(exclude_file) )
	for line in open(exclude_file,'r'):
		line = line.strip()
		exclude_list[line] = True
	sys.stderr.write("# Found {} entries to exclude\n".format( len(exclude_list) ) )
	
	matrix_csv = sys.argv[2]
	#matrix_csv = csv.reader( open(sys.argv[2],'r'), skipinitialspace=True)
	#filtered_csv = csv.writer( sys.stdout, lineterminator=os.linesep)
	sys.stderr.write("# Reading {}\n".format( sys.argv[2] ) )
	for line in open(matrix_csv,'r'):
		if line.strip():
			lsplits = line.split("\t")
			line_count += 1
			species = lsplits[2]
			if species=="Species": # meaning header line
				sys.stdout.write(line)
				#filtered_csv.writerow(lsplits)
				continue
			
			# split  "ANME-1 cluster archaeon"  into  ["ANME-1", "cluster", "archaeon"]
			# and search each split in the exclude list
			for species_split in species.split(" "):
				if species_split in exclude_list:
					remove_count += 1
					break
			else:
				sys.stdout.write(line)
				#filtered_csv.writerow(lsplits)
	sys.stderr.write("# Found {} lines and removed {}\n".format( line_count, remove_count ) )


