# subsurface2017

data for:

Orsi, WD, TA. Richards, WR. Francis (2018) [Predicted microbial secretomes and their target substrates in marine sediment](https://doi.org/10.1038/s41564-017-0047-9). **Nature Microbiology** 3(1):32-37.

![Figure_1_AB.png](https://bitbucket.org/wrf/subsurface2017/raw/9a031dc14d6002609c4eafad833317649f3fc1cb/v1-figs/Figure_1_AB.png)

Raw metatranscriptome reads, originally from [Orsi et al 2013](https://www.ncbi.nlm.nih.gov/pubmed/23760485), can be found at NCBI BioProject [PRJNA175360](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA175360)

## files ##
analysis:

* [blast_to_gene_matrix.py](https://bitbucket.org/wrf/subsurface2017/src/master/blast_to_gene_matrix.py) - script to integrate results from BLAST to various databases, etc
* [all_hits_signalp_from_db_BLASTp_against_CAZY_blastmatrix.txt](https://bitbucket.org/wrf/subsurface2017/src/master/all_hits_signalp_from_db_BLASTp_against_CAZY_blastmatrix.txt) - final matrix, as tab-delimited file

proteins:

* [all_PM__ORFS.faa.gz](https://bitbucket.org/wrf/subsurface2017/downloads/all_PM__ORFs.faa.gz) - combined ORFs from all samples (all depths), [in downloads](https://bitbucket.org/wrf/subsurface2017/downloads/)
