#!/usr/bin/env python
#
# split_squeezemeta_tax_to_columns.py

"""split_squeezemeta_tax_to_columns.py  last modified 2022-04-04

./split_squeezemeta_tax_to_columns.py 13.NAMIBIA.orftable > 13.NAMIBIA.orftable.w_taxa

"""

# table headers are:
#0      1           2           3       4           5           6       7           8   9       10      11          12      13      14      15      16
#ORF ID	Contig ID	Molecule	Method	Length NT	Length AA	GC perc	Gene name	Tax	KEGG ID	KEGGFUN	KEGGPATH	COG ID	COGFUN	COGPATH	PFAM	TPM Sample1
#
# long taxon lists may include:
#
# k_Eukaryota;n_Opisthokonta;n_Metazoa;n_Eumetazoa;n_Bilateria;n_Deuterostomia;
# p_Chordata;n_Craniata;n_Vertebrata;n_Gnathostomata;n_Teleostomi;n_Euteleostomi;
# superc_Sarcopterygii;n_Dipnotetrapodomorpha;n_Tetrapoda;n_Amniota;n_Mammalia;n_Theria;n_Eutheria;n_Boreoeutheria;
# supero_Euarchontoglires;n_Glires;n_Rodentia;n_Myomorpha;n_Muroidea;f_Muridae;n_Murinae;g_Rattus;s_Rattus norvegicus
#
# k_Eukaryota;n_Opisthokonta;n_Metazoa;n_Eumetazoa;n_Bilateria;n_Protostomia;n_Ecdysozoa;n_Panarthropoda;
# p_Arthropoda;n_Mandibulata;n_Pancrustacea;n_Hexapoda;
# c_Insecta;n_Dicondylia;n_Pterygota;n_Neoptera;n_Holometabola;
# o_Diptera;n_Nematocera;n_Culicomorpha;superf_Chironomoidea;n_Ceratopogonidae;n_Ceratopogoninae;n_Culicoidini;g_Culicoides
#
# k_Eukaryota;n_Viridiplantae;
# p_Streptophyta;n_Streptophytina;n_Embryophyta;n_Tracheophyta;n_Euphyllophyta;n_Spermatophyta;
# c_Magnoliopsida;n_Mesangiospermae;n_eudicotyledons;n_Gunneridae;n_Pentapetalae;n_rosids;n_fabids;
# o_Fabales;f_Fabaceae;n_Papilionoideae;n_50 kb inversion clade;n_genistoids sensu lato;n_core genistoids;n_Genisteae;g_Lupinus

import sys

kingdom_level = ["Viridiplantae", "Metazoa", "Rhizaria", "Fungi", "Alveolata", "Stramenopiles", "Amoebozoa", "Discoba", "Rhodophyta"]
# c_Cryptophyceae n_Apusozoa n_Metamonada p_Haptista c_Ichthyosporea

if len(sys.argv) < 2:
	sys.exit(__doc__)
else:
	tablefile = sys.argv[1]

	linecounter = 0
	wtaxa_counter = 0
	notaxa_counter = 0

	sys.stderr.write("# reading table {}\n".format(tablefile) )
	for line in open(tablefile,'r'):
		line = line.strip()
		if line:
			linecounter += 1
			if line[0]=="#":
				print(line)
				continue
			lsplits = line.split("\t")
			contig_id = lsplits[1]
			if contig_id=="Contig ID":
				newline = "{}\tdomain\tkingdom\tphylum\tclass\torder\tfamily\tgenus\tspecies".format(line)
				print(newline)
				continue
			taxa = lsplits[8].strip()
			taxadict = {}
			ncounts = 1
			if taxa:
				wtaxa_counter += 1
				taxalist = taxa.split(";")
				for taxon in taxalist:
					if taxon=="c_Choanoflagellata":
						taxadict["k"] = "Choanoflagellata"
					taxcode, taxname = taxon.split("_",1)
					if taxcode=="n":
						taxcode = "n{}".format(ncounts)
						ncounts += 1
					if taxcode=="k":
						if taxname=="Archaea" or taxname=="Bacteria":
							taxadict["k"] = taxname
						taxcode = "d" # not kingdoms, domains of life

					# assign pseudokingdoms
					if taxname in kingdom_level:
						taxadict["k"] = taxname

					# give sub level to super level
					if taxcode=="superc":
						taxadict["c"] = taxname
					if taxcode=="supero":
						taxadict["o"] = taxname
					if taxcode=="superf":
						taxadict["f"] = taxname

					#
					taxadict[taxcode] = taxname
				newline = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}".format( line, taxadict.get("d","none"), taxadict.get("k","none"), taxadict.get("p","none"), taxadict.get("c","none"), taxadict.get("o","none"), taxadict.get("f","none"), taxadict.get("g","none"), taxadict.get("s","none") )
			else:
				notaxa_counter += 1
				newline = "{}\tNA\tNA\tNA\tNA\tNA\tNA\tNA\tNA".format(line)
			print(newline)
	sys.stderr.write("# found {} lines, {} w taxa, {} none\n".format(linecounter, wtaxa_counter, notaxa_counter) )


#